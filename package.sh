#!/bin/sh
rm -rf node_modules/ && npm install --production
zip -r package.zip src/ node_modules/
echo "Package created at ./package.zip"
