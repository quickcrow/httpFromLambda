# Endpoint

http://lambda-alb-1444610034.eu-west-2.elb.amazonaws.com/ *(currently disabled)*

# About

On November 29th 2018 AWS announced that: [Application Load Balancer can now Invoke Lambda Functions to Serve HTTP(S) Requests.](https://aws.amazon.com/about-aws/whats-new/2018/11/alb-can-now-invoke-lambda-functions-to-serve-https-requests/?sc_channel=em&sc_campaign=GLOBAL_LA_re:Invent-Recap-3_20181130&sc_medium=em_127225&sc_content=PA_nl_la&sc_geo=mult&sc_country=mult&sc_outcome=pa&trk=em_127225&mkt_tok=eyJpIjoiTjJReE1tTmxZamN4WmpnNCIsInQiOiJVZ3lyWDNSSFwvS2RMWEFcLzlOd0p0VnNIcnV5V1wvMzR1K1M4QXJmZjEzZXNIcXVIZFN4cVdoRVJ2Z29nMWJYeG5Xc0p4VDJMSFI1MVE4T0dRVU5PWWVnQjM1cmtZMWM2MzVVanNvUklsQ29hTVJBRnk0SFVcL1VuWmRpNWVxQjVHYjBKT0xxeUZuV3ZiREN4U1dZTngrXC84UT09In0%3D)


I wanted to see if I could get an AWS Lambda to serve a static web page directly from an ALB request, essentially enabling a very simple website infrastructure hosted on serverless billed by the 100ms of invocation time per request!

# Notes

* [CloudFormation currently (1st December 2018) does not seem to support lambda target type on target groups](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-resource-elasticloadbalancingv2-targetgroup.html#cfn-elasticloadbalancingv2-targetgroup-targettype), so infra/cloudformation.yaml is not yet fully functional. The current solution involved deploying this stack, and then creating a lambda type target group and attaching to the application load balancer manually through the console.
* [Explains expected response object for lambda callback](https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-output-format).
