'use strict';

const index = require('../src/index');
const req = require('./exampleRequest.json');
const context = require('./exampleRequest.json');

index.handler(req, context, (err, res) => {
    console.log(`err:${JSON.stringify(err, null, 2)}`);
    console.log(`res:${JSON.stringify(res, null, 2)}`);
});