'use strict';

exports.handler = (event, context, callback) => {
    callback(null, {
        "statusCode": 200,
        "headers": {
            "Content-Type": "text/html"
        },
        "body": "<html>Hello from AWS lambda!</html>",
        "isBase64Encoded": false
    });
};